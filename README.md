# Navigation_EP

La présentation générale de Spip n'est pas modifiée. Le jeu de squelettes ne modifie que la barre de menu dans l'espace privé ce qui rend le contenu de l'espace privé plus lisible et accessible sur smartphone.
    
    Général :
    - les couleurs sont adaptées aux préférences de l'utilisateur.
    - la barre de menu est en 2 parties : horizontale haut de page et fixe pour la présentation de l'espace privé, verticale gauche pour les menus 
    - l'icône de l'auteur fait apparaître au survol un dropdown contenant les infos de l'auteur et la déconnexion.
    - le texte "Espace privé" est cliquable et ramène à la page d'accueil de l'espace privé.
    - l'icône du site est cliquable pour accéder à l'interface public.
    - une flèche apparait en bas de page lors du défilement de la page pour directement revenir en haut. 
    - un mode jour/nuit automatique.

    Sur petits écrans style smartphone : 
    - la barre de navigation s'adapte au format de l'écran avec un bouton menu. 
    - les colonnes "navigation (infos, publier, docs...)" et "extras" sont accessibles via des boutons switch dans des blocs dépliables (repliés par défaut à l'ouverture d'une page). 
    - un bouton en bas de page à droite (fixe) regroupe les outils de création rapide.

    Problème connu :
    Le menu est inactif sur la page principale (uniquement) SVP d'administration des plugins. Afin de pallier à ce problème, le texte "Espace privé" (barre de menu horizontale haut fixe) est cliquable et ramène à la page d'accueil de l'espace privé. 
    
    Librairies intégrées :
    - tailwind CSS v3.3.6
    - flowbite v2.2.0
    - TW elements v1.1.0

    A faire: internationaliser 